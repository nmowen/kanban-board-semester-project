INSERT INTO status (name) VALUES ('Active');
INSERT INTO status (name) VALUES ('Inactive');

INSERT INTO stage (name) VALUES ('Pre-production');
INSERT INTO stage (name) VALUES ('Production');
INSERT INTO stage (name) VALUES ('Close Out');
INSERT INTO stage (name) VALUES ('Archived');

INSERT INTO employee (name) VALUES ('Montgomery Scott');
INSERT INTO employee (name) VALUES ('Amelia Bedelia');
INSERT INTO employee (name) VALUES ('Alisha Jones');
INSERT INTO employee (name) VALUES ('Marisol Fuentes');
INSERT INTO employee (name) VALUES ('Geordi La Forge');
INSERT INTO employee (name) VALUES ('Molly O''Reilly');
INSERT INTO employee (name) VALUES ('Saul Goodman');
INSERT INTO employee (name) VALUES ('Jesse');
INSERT INTO employee (name) VALUES ('James');
INSERT INTO employee (name) VALUES ('WOBBUFFET!');

INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Innovative Solutions','101 Zoey St.','TX','Houston','77018');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Helios Energy','2323 Garden Dr.','TX','Houston','77036');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Apollon Media','166 Maven St.','TX','El Paso','79835');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Helperides Finance','5 Jackson Blvd.','TX','Houston','77071');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Kronos Foods','3341 Grandview St.','TX','Houston','77050');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Odysseus Communications','867 Camino Rd.','TX','Houston','77223');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Poseidon Line','2779 Eagle Blvd.','TX','Spring','77373');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Titan Industry','6765 Spring St.','CA','Napa','94558');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('University of Houston','4800 Calhoun Rd.','TX','Houston','77004');
INSERT INTO customer (name,address,state_abbr,city,zip_code) VALUES ('Jack''s Car Shack','2213 Example Ave.','TX','Houston','77256');

INSERT INTO public.component (name, price) VALUES ('A-Arm', 1500.00);
INSERT INTO public.component (name, price) VALUES ('Band Brake Drum', 930.00);
INSERT INTO public.component (name, price) VALUES ('Bearing Bracket', 2103.00);
INSERT INTO public.component (name, price) VALUES ('Disk Brake Caliper', 2000.00);
INSERT INTO public.component (name, price) VALUES ('Brake Control Lever', 2100.00);
INSERT INTO public.component (name, price) VALUES ('Piston Head', 1400.00);
INSERT INTO public.component (name, price) VALUES ('Engine Block', 11500.00);
INSERT INTO public.component (name, price) VALUES ('Pulley', 1000.00);
INSERT INTO public.component (name, price) VALUES ('Crank Shaft', 3400.00);
INSERT INTO public.component (name, price) VALUES ('Gripper Rod Center', 2000.00);

INSERT INTO job (name, employee_id) VALUES ('Suspension Assembly', 4);
INSERT INTO job (name, employee_id) VALUES ('Vehicle Brake', 1);
INSERT INTO job (name, employee_id) VALUES ('Transformer', 2);
INSERT INTO job (name, employee_id) VALUES ('Pulley System', 2);
INSERT INTO job (name, employee_id) VALUES ('Engine Rebuild', 3);
INSERT INTO job (name, employee_id) VALUES ('Crank Shaft', 6);
INSERT INTO job (name, employee_id) VALUES ('Transmission', 7);
INSERT INTO job (name, employee_id) VALUES ('Brake Caliper Set', 8);
INSERT INTO job (name, employee_id) VALUES ('Rods', 9);
INSERT INTO job (name, employee_id) VALUES ('Truck Brake Levers', 5);

INSERT INTO job_component_line (job_id, component_id) VALUES (1, 1);
INSERT INTO job_component_line (job_id, component_id) VALUES (2, 2);
INSERT INTO job_component_line (job_id, component_id) VALUES (3, 10);
INSERT INTO job_component_line (job_id, component_id) VALUES (4, 8);
INSERT INTO job_component_line (job_id, component_id) VALUES (5, 7);
INSERT INTO job_component_line (job_id, component_id) VALUES (6, 9);
INSERT INTO job_component_line (job_id, component_id) VALUES (7, 3);
INSERT INTO job_component_line (job_id, component_id) VALUES (8, 4);
INSERT INTO job_component_line (job_id, component_id) VALUES (9, 10);
INSERT INTO job_component_line (job_id, component_id) VALUES (10, 5);

