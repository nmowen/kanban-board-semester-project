CREATE TABLE public.employee
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50)
);

CREATE TABLE public.job
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  employee_id INT NOT NULL REFERENCES employee(id)
);

CREATE TABLE public.component
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  price DECIMAL(10,2) NOT NULL
);

CREATE TABLE public.job_component_line
(
  id SERIAL PRIMARY KEY NOT NULL,
  job_id INT NOT NULL REFERENCES job(id),
  component_id INT NOT NULL REFERENCES component(id)
);
CREATE TABLE public.status
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(20)
);
CREATE TABLE public.stage
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(20)
);
CREATE TABLE public.customer
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  address VARCHAR(200) NOT NULL,
  city VARCHAR(50) NOT NULL,
  state_abbr CHAR(2) NOT NULL,
  zip_code VARCHAR(10) NOT NULL,
  status_id INT REFERENCES status(id)
);