ALTER TABLE public.component ADD COLUMN status_id INT REFERENCES status(id);
ALTER TABLE public.customer ALTER COLUMN status_id SET DEFAULT 1;
ALTER TABLE public.employee ADD COLUMN status_id INT REFERENCES status(id);
ALTER TABLE public.employee ALTER COLUMN status_id SET DEFAULT 1;
