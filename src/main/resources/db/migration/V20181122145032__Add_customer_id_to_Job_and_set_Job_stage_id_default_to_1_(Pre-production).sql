ALTER TABLE public.job ADD COLUMN stage_id INT REFERENCES stage(id);
ALTER TABLE public.job ALTER COLUMN stage_id SET DEFAULT 1;
ALTER TABLE public.job ADD COLUMN customer_id INT REFERENCES customer(id);

/*
before trying to insert any records into tables w/ the stage_id or status_id FKs,
make sure u populate the lookups!! -nicole

INSERT INTO status (name) VALUES ('Active');
INSERT INTO status (name) VALUES ('Inactive');

INSERT INTO stage (name) VALUES ('Pre-production');
INSERT INTO stage (name) VALUES ('Production');
INSERT INTO stage (name) VALUES ('Close Out');
INSERT INTO stage (name) VALUES ('Archived');
 */