package edu.uh.tech.cis3368.kanbanproject;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Component
public class ComponentsController implements Initializable {

    public ComponentsController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void newComponentButtonClicked(MouseEvent mouseEvent) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("component_record.fxml"));
        stage.setTitle("New Component Record");
        stage.setScene(new Scene(root,600,271));
        stage.show();
    }
}
