package edu.uh.tech.cis3368.kanbanproject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Integer> {
   //@Override
   //Iterable<EmployeeEntity> findAll();

//    @Override
//    List<EmployeeEntity> findAll();

}
