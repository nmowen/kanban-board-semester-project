package edu.uh.tech.cis3368.kanbanproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class JobsController implements Initializable {

    @Autowired
    private JobEntityRepository jobEntityRepository;

    private static final DataFormat JOB_LIST =  new DataFormat(new String[]{"cis3368/personList"});
    public ListView<JobEntity> preProductionList;
    public ListView<JobEntity> productionList;
    public ListView<JobEntity> closeOutList;
    public ListView<JobEntity> archivedList;
    public ListView sourceList;

    public JobsController() {
    }

    public void preProdDragDetected(MouseEvent mouseEvent) {
        System.out.println("Drag event detected");
        int selected = this.preProductionList.getSelectionModel().getSelectedIndices().size();
        System.out.println(String.format("%d selected", selected));
        if (selected > 0) {
            Dragboard dragboard = this.preProductionList.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<JobEntity> selectedItems = new ArrayList(this.preProductionList.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(JOB_LIST, selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }
    }

    public void preProdDragDone(DragEvent dragEvent) {
        System.out.println("Drag done detected");
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if (tm == TransferMode.MOVE) {
            this.removeSelectedItemsFromPreProduction();
        }

        dragEvent.consume();
    }

    public void preProdDragDropped(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            ArrayList<JobEntity> people = (ArrayList)dragboard.getContent(JOB_LIST);
            this.preProductionList.getItems().addAll(people);
            dragCompleted = true;
        }

        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    public void preProdDragOver(DragEvent dragEvent) {
        System.out.println("Drag over detected");
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            dragEvent.acceptTransferModes(new TransferMode[]{TransferMode.MOVE});
        }

        dragEvent.consume();
    }

    public void prodDragDetected(MouseEvent mouseEvent) {
        System.out.println("Drag event detected");
        int selected = this.productionList.getSelectionModel().getSelectedIndices().size();
        System.out.println(String.format("%d selected", selected));
        if (selected > 0) {
            Dragboard dragboard = this.productionList.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<JobEntity> selectedItems = new ArrayList(this.productionList.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(JOB_LIST, selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }
    }

    public void prodDragDone(DragEvent dragEvent) {
        System.out.println("Drag done detected");
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if (tm == TransferMode.MOVE) {
            this.removeSelectedItemsFromProduction();
        }

        dragEvent.consume();
    }

    public void prodDragDropped(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            ArrayList<JobEntity> people = (ArrayList)dragboard.getContent(JOB_LIST);
            this.productionList.getItems().addAll(people);
            dragCompleted = true;
        }

        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    public void prodDragOver(DragEvent dragEvent) {
        System.out.println("Drag over detected");
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            dragEvent.acceptTransferModes(new TransferMode[]{TransferMode.MOVE});
        }

        dragEvent.consume();
    }

    public void closeOutDragDetected(MouseEvent mouseEvent) {
        System.out.println("Drag event detected");
        int selected = this.closeOutList.getSelectionModel().getSelectedIndices().size();
        System.out.println(String.format("%d selected", selected));
        if (selected > 0) {
            Dragboard dragboard = this.closeOutList.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<JobEntity> selectedItems = new ArrayList(this.closeOutList.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(JOB_LIST, selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }
    }

    public void closeOutDragDrone(DragEvent dragEvent) {
        System.out.println("Drag done detected");
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if (tm == TransferMode.MOVE) {
            this.removeSelectedItemsFromCloseOut();
        }

        dragEvent.consume();
    }

    public void closeOutDragDropped(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            ArrayList<JobEntity> people = (ArrayList)dragboard.getContent(JOB_LIST);
            this.closeOutList.getItems().addAll(people);
            dragCompleted = true;
        }

        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    public void closeOutDragOver(DragEvent dragEvent) {
        System.out.println("Drag over detected");
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            dragEvent.acceptTransferModes(new TransferMode[]{TransferMode.MOVE});
        }

        dragEvent.consume();
    }

    public void archivedDragDetected(MouseEvent mouseEvent) {
        System.out.println("Drag event detected");
        int selected = this.archivedList.getSelectionModel().getSelectedIndices().size();
        System.out.println(String.format("%d selected", selected));
        if (selected > 0) {
            Dragboard dragboard = this.archivedList.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<JobEntity> selectedItems = new ArrayList(this.archivedList.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(JOB_LIST, selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }
    }

    public void archivedDragDone(DragEvent dragEvent) {
        System.out.println("Drag done detected");
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if (tm == TransferMode.MOVE) {
            this.removeSelectedItemsFromArchived();
        }

        dragEvent.consume();
    }

    public void archivedDragDropped(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            ArrayList<JobEntity> people = (ArrayList)dragboard.getContent(JOB_LIST);
            this.archivedList.getItems().addAll(people);
            dragCompleted = true;
        }

        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    public void archivedDragOver(DragEvent dragEvent) {
        System.out.println("Drag over detected");
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(JOB_LIST)) {
            dragEvent.acceptTransferModes(new TransferMode[]{TransferMode.MOVE});
        }

        dragEvent.consume();
    }

    public void newJobMouseClicked(MouseEvent mouseEvent) throws IOException {
        //root = FXMLLoader.load(getClass().getResource("job_record.fxml"));
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("job_record.fxml"));
        stage.setTitle("New Job Record");
        stage.setScene(new Scene(root,600,350));
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //System.out.println("initialized");
        this.preProductionList.getItems().addAll(this.buildInitialData());
        this.preProductionList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    private ObservableList<JobEntity> buildInitialData() {
        ObservableList<JobEntity> list = FXCollections.observableArrayList();
        String[] names = new String[]{"Job1", "Job2", "Job3", "Job4", "Job5", "Job6"};
        int id = 1;
        String[] var4 = names;
        int var5 = names.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            String name = var4[var6];
            list.add(new JobEntity(id++, name));
        }

        //ASK ABT THESE GOD DANG DOUBLE TO BIGDECIMAL CONVERSIONS IN CLASS
//        ObservableList<ComponentEntity> component_list = FXCollections.observableArrayList();
//        String[] component_names = new String[]{"Component1","Component2","Component3"};
//        Double[] prices = new Double[]{10.00,22.25,33.50};
//        BigDecimal[] component_prices = new BigDecimal[]{};
//        for (int i=0; i < prices.length; ++i) {
//            component_prices[i] = prices[i];
//        }
//        int component_id = 1;
//        String[] var7 = component_names;
//        int var9 = component_names.length;
//        BigDecimal[] var8 = component_prices;
//        for (int var6=0; var6 < var9; ++var6) {
//            String name = var7[var6];
//            BigDecimal price = var8[var6];
//            component_list.add(new ComponentEntity(id++, name, price));
//        }

        return list;
    }

    //on behalf of my project group i'd like to welcome you to the spaghetti warehouse.
    //Ooh my god just change this to one function with listview references if theres time at the end.
    private void removeSelectedItemsFromPreProduction() {
        List<JobEntity> selectedFolk = new ArrayList();
        Iterator var2 = this.preProductionList.getSelectionModel().getSelectedItems().iterator();

        while(var2.hasNext()) {
            JobEntity person = (JobEntity)var2.next();
            selectedFolk.add(person);
        }

        this.preProductionList.getSelectionModel().clearSelection();
        this.preProductionList.getItems().removeAll(selectedFolk);
    }

    private void removeSelectedItemsFromProduction() {
        List<JobEntity> selectedFolk = new ArrayList();
        Iterator var2 = this.productionList.getSelectionModel().getSelectedItems().iterator();

        while(var2.hasNext()) {
            JobEntity person = (JobEntity)var2.next();
            selectedFolk.add(person);
        }

        this.productionList.getSelectionModel().clearSelection();
        this.productionList.getItems().removeAll(selectedFolk);
    }

    private void removeSelectedItemsFromCloseOut() {
        List<JobEntity> selectedFolk = new ArrayList();
        Iterator var2 = this.closeOutList.getSelectionModel().getSelectedItems().iterator();

        while(var2.hasNext()) {
            JobEntity person = (JobEntity)var2.next();
            selectedFolk.add(person);
        }

        this.closeOutList.getSelectionModel().clearSelection();
        this.closeOutList.getItems().removeAll(selectedFolk);
    }

    private void removeSelectedItemsFromArchived() {
        List<JobEntity> selectedFolk = new ArrayList();
        Iterator var2 = this.archivedList.getSelectionModel().getSelectedItems().iterator();

        while(var2.hasNext()) {
            JobEntity person = (JobEntity)var2.next();
            selectedFolk.add(person);
        }

        this.archivedList.getSelectionModel().clearSelection();
        this.archivedList.getItems().removeAll(selectedFolk);
    }

    public void jobSelect(ContextMenuEvent contextMenuEvent) {
        //add code for selecting & displaying the proper job record info in job_record.fxml HERE
    }


   /* @FXML
    private Label myLabel;

    public void printToConsole(ActionEvent actionEvent) {
        System.out.println("HelloWorld");
    }*/


}
