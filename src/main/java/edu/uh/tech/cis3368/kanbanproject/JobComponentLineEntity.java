package edu.uh.tech.cis3368.kanbanproject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "job_component_line", schema = "public", catalog = "kanban")
public class JobComponentLineEntity {
    private int id;
    private ComponentEntity component;
    private JobEntity job;

    public JobComponentLineEntity() {
    }

    public JobComponentLineEntity(JobEntity job, ComponentEntity component) {
        this.component = component;
        this.job = job;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobComponentLineEntity that = (JobComponentLineEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "component_id", referencedColumnName = "id")
    public ComponentEntity getComponent() {
        return component;
    }

    public void setComponent(ComponentEntity componentByComponentId) {
        this.component = componentByComponentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    public JobEntity getProduct() {
        return job;
    }

    public void setProduct(JobEntity productByProductId) {
        this.job = productByProductId;
    }
}
