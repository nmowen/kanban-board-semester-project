package edu.uh.tech.cis3368.kanbanproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;


@Component
public class EmployeesController implements Initializable {

    @FXML
    private ListView<EmployeeEntity> employeeList;

    @FXML
    private Button newEmployeeButton;

    @Autowired
    private EmployeeRepository employeeRepository;

    private static final DataFormat EMPLOYEE_LIST = new DataFormat("cis3368/employeeList");
    private EmployeeEntity employee;

    public EmployeesController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.employeeList.getItems().addAll(loadData());
        this.employeeList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


    }

    public void newEmployeesButtonClicked(MouseEvent mouseEvent) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("employee_record.fxml"));
        stage.setTitle("New Employee Record");
        stage.setScene(new Scene(root, 600, 216));
        stage.show();
    }

    private ObservableList<EmployeeEntity> loadData() {
        ObservableList<EmployeeEntity> list = FXCollections.observableArrayList();

        //IDK

        return list;
    }

}