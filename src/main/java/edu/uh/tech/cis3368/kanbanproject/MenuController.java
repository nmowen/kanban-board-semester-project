package edu.uh.tech.cis3368.kanbanproject;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Component
public class MenuController implements Initializable {


    @FXML
    private Button customersButton;

    @FXML
    private Button componentsButton;

    @FXML
    private Button employeesButton;

    @FXML
    private Button jobsButton;

    public MenuController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //System.out.println("initialized");
    }


    public void customersClicked(MouseEvent mouseEvent) throws IOException {
        //root = FXMLLoader.load(getClass().getResource("job_record.fxml"));
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("customers.fxml"));
        stage.setTitle("Customer Records");
        stage.setScene(new Scene(root,659,400));
        stage.show();
    }

    public void componentsClicked(MouseEvent mouseEvent) throws IOException {
        //root = FXMLLoader.load(getClass().getResource("job_record.fxml"));
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("components.fxml"));
        stage.setTitle("Component Records");
        stage.setScene(new Scene(root,409,400));
        stage.show();
    }

    public void employeesClicked(MouseEvent mouseEvent) throws IOException {
        //root = FXMLLoader.load(getClass().getResource("job_record.fxml"));
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("employees.fxml"));
        stage.setTitle("Employee Records");
        stage.setScene(new Scene(root,409,400));
        stage.show();

    }

    public void jobsClicked(MouseEvent mouseEvent) throws IOException {
        //root = FXMLLoader.load(getClass().getResource("job_record.fxml"));
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("jobs.fxml"));
        stage.setTitle("Job Records");
        stage.setScene(new Scene(root,888,400));
        stage.show();
    }
}
