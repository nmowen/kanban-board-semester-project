package edu.uh.tech.cis3368.kanbanproject;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Component
public class JobRecordController implements Initializable {

    private JobEntityRepository jobEntityRepository;

    public JobRecordController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void addJobComponentsButtonClicked(MouseEvent mouseEvent) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("job_component.fxml"));
        stage.setTitle("Job Components");
        stage.setScene(new Scene(root,600,413));
        stage.show();
    }
}
