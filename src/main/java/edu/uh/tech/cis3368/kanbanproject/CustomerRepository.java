package edu.uh.tech.cis3368.kanbanproject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Integer> {
    //@Override
    //Iterable<CustomerEntity> findAll();

//    @Override
//    List<CustomerEntity> findAll();

    //ArrayList<CustomerEntity> findAll();
}
