package edu.uh.tech.cis3368.kanbanproject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "job", schema = "public", catalog = "kanban")
public class JobEntity implements Serializable {
    private int id;
    private String name;
    private List<JobComponentLineEntity> jobComponents = new ArrayList<>();

    public JobEntity() {
    }

    public JobEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobEntity jobEntity = (JobEntity) o;
        return id == jobEntity.id &&
                Objects.equals(name, jobEntity.name);
    }

    @Override
    public String toString() {

        return this.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<JobComponentLineEntity> getProductComponents() {
        return jobComponents;
    }

    public void setProductComponents(List<JobComponentLineEntity> productComponents) {
        this.jobComponents = productComponents;
    }

    public void addComponent(ComponentEntity component) {
        JobComponentLineEntity productComponent = new JobComponentLineEntity( this, component );
        jobComponents.add(productComponent);
        component.getJobComponents().add(productComponent);

    }

    public void removeComponent(ComponentEntity component) {
        for (Iterator<JobComponentLineEntity> iterator = jobComponents.iterator(); iterator.hasNext(); ) {
            JobComponentLineEntity productComponent = iterator.next();
            if (productComponent.getComponent().equals(component) && productComponent.getProduct().equals(this)) {
                iterator.remove();
                productComponent.getComponent().getJobComponents().remove(productComponent);
                break;

            }
        }
    }
}
