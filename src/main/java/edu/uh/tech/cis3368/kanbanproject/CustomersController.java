package edu.uh.tech.cis3368.kanbanproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Component
public class CustomersController implements Initializable {

    @FXML
    private TableView<CustomerEntity> customersTable;

    @FXML
    private TableColumn<CustomerEntity, Integer> customerID;

    @FXML
    private TableColumn<CustomerEntity, String> customerName;

    @FXML
    private TableColumn<CustomerEntity, String> customerAddress;

    @FXML
    private TableColumn<CustomerEntity, String> customerCity;

    @FXML
    private TableColumn<CustomerEntity, String> customerState;

    @FXML
    private TableColumn<CustomerEntity, String> customerZip;

    @FXML
    private TableColumn<CustomerEntity, Integer> customerStatus;

    @FXML
    private Button newCustomerButton;

    @Autowired
    private CustomerRepository customerRepository;

    private ObservableList<CustomerEntity> data = FXCollections.observableArrayList();


    public CustomersController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setColumns();
        loadData();
    }

    public void newCustomerButtonClicked(MouseEvent mouseEvent) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("customer_record.fxml"));
        stage.setTitle("New Customer Record");
        stage.setScene(new Scene(root,579,319));
        stage.show();
    }

    public void setColumns() {
        customerID.setCellValueFactory(new PropertyValueFactory<CustomerEntity, Integer>("ID"));
        customerName.setCellValueFactory(new PropertyValueFactory<CustomerEntity, String>("Name"));
        customerAddress.setCellValueFactory(new PropertyValueFactory<CustomerEntity, String>("Address"));
        customerCity.setCellValueFactory(new PropertyValueFactory<CustomerEntity, String>("City"));
        customerZip.setCellValueFactory(new PropertyValueFactory<CustomerEntity, String>("Zip"));
        customerState.setCellValueFactory(new PropertyValueFactory<CustomerEntity, String>("State"));
        customerStatus.setCellValueFactory(new PropertyValueFactory<CustomerEntity, Integer>("Status"));

    }

    public void loadData() {
        data.clear();

        // NOT WORKING .....
        //data.addAll(customerRepository.findAll());


//        Iterable<CustomerEntity> iterable = customerRepository.findAll();
//        for (CustomerEntity customers : iterable) {
//
//            data.addAll(customers);
//        }
//
//        Iterable<CustomerEntity> iterable = customerRepository.findAll();
//        for (CustomerEntity customer : iterable) {
//            //TO DO
//         }

//        for (CustomerEntity data1 :data) {
//            data.add(new CustomerEntity(data1.getId(),data1.getName()));
//        }

        customersTable.setItems(data);

    }
}
