package edu.uh.tech.cis3368.kanbanproject;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "component", schema = "public", catalog = "kanban")
public class ComponentEntity {
    private int id;
    private String name;
    private BigDecimal price;
    private List<JobComponentLineEntity> jobComponents = new ArrayList<>();

    public ComponentEntity() {
    }

    public ComponentEntity(int id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price", nullable = false, precision = 2)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentEntity that = (ComponentEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, price);
    }

    @OneToMany(mappedBy = "component")
    public List<JobComponentLineEntity> getJobComponents() {
        return jobComponents;
    }
    public void setJobComponents(List<JobComponentLineEntity> jobComponents) {
        this.jobComponents = jobComponents;
    }
}
