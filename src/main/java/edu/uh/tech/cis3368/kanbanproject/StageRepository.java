package edu.uh.tech.cis3368.kanbanproject;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StageRepository extends CrudRepository<StageEntity, Integer> {
}
