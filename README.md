# Kanban Board Semester Project

This application allows users to manage the production process for a small manufacturing shop.

### Definitions
**Shop owner** - the person who enters the initial data in the application

**Shop manager** - the person who manages the production process

**Workers** - one or more people responsible for assembly, etc.

**Customer** - the person requesting a custom product

## Team Members
* Edreih Aldana
* Nicole Owen

## Technologies Used
* SpringBoot
* PostgreSQL
* JavaFX
* Flyway
* Maven
* Java 10
* Amazon S3
* ICON PACK: https://www.flaticon.com/packs/manufacturing-4